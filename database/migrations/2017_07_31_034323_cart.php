<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->string('identifier', 50);
            // зарезервировано на будущее, на случай нужды в мультикорзины
            $table->string('type', 50)->nullable();
            $table->longText('order_data')->comment('Детали заказа');

            $table->unique(['identifier', 'type'], 'cart_user_identifier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
