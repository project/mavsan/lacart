<?php
/**
 * BuyableModelProduct.php
 * Date: 01.08.2017
 * Time: 10:49
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\test\Fixtures;


use Mavsan\LaCart\Interfaces\Buyable;

class BuyableModelProduct extends ModelProduct implements Buyable
{
    protected $id;

    protected $title;

    protected $price;

    /**
     * BuyableModelProduct constructor.
     *
     * @param $id
     * @param $title
     * @param $price
     */
    public function __construct(
        $id = 1,
        $title = 'Product title',
        $price = 99.99
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
    }

    /**
     * Получение идентификатора товара
     * @return int|string
     */
    public function cartGetID()
    {
        return $this->id;
    }

    /**
     * Получение стоимости товара
     * @return double
     */
    public function cartGetPrice()
    {
        return $this->price;
    }

    /**
     * Получение названия товара
     * @return string
     */
    public function cartGetTitle()
    {
        return $this->title;
    }

}