<?php
/**
 * CartDicsount.php
 * Date: 04.08.2017
 * Time: 10:10
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\test\Fixtures;


class CartDiscount
{
    public static function one()
    {
        \App::make('cartDiscount')->add(10, 11, 12);
    }

    public function two()
    {
        \App::make('cartDiscount')->add(20, 21, 22);
    }

    public function three()
    {
        \App::make('cartDiscount')->add(30, 31, 32);
    }
}