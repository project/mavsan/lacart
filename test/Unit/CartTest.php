<?php
/**
 * CartTest.php
 * Date: 01.08.2017
 * Time: 10:16
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\test;

use Mavsan\LaCart\Exceptions\CartItemModelNotFoundException;
use Mavsan\LaCart\Models\Cart;
use Mavsan\LaCart\Models\CartDiscount;
use Mavsan\LaCart\test\Fixtures\BuyableModelProduct;
use Mavsan\LaCart\test\Fixtures\ModelProduct;
use Tests\TestCase;

class CartTest extends TestCase
{
    public function testCreate()
    {
        $this->assertNull(\Session::get(config('cart.instanceSessionName')));

        $cart = $this->getCart();

        $this->assertInstanceOf(\Mavsan\LaCart\Models\Cart::class, $cart);
    }

    /**
     * @return \Mavsan\LaCart\Models\Cart
     */
    protected function getCart()
    {
        return \App::make('cart');
    }

    public function testGetItems()
    {
        $cart = $this->getCart();
        $items = $cart->getItems();

        $this->assertInstanceOf(\Illuminate\Support\Collection::class, $items);
    }

    public function testAdd()
    {
        $cart = $this->getCart();

        $this->assertCount(0, $cart->getItems());

        $this->expectsEvents('cart.added');

        // товар 1
        $cartItem = $cart->add(18, 'tp', 2, 15.3, []);

        $this->assertCount(1, $cart->getItems());

        // товар 1, только с опциями, должен добавиться отдельно
        $cartItem1 = $cart->add(18, 'tp', 5, 15.3, ['some' => 'opt']);

        $this->assertCount(2, $cart->getItems());

        // товар 1 снова добавили, должно увеличиться кол-во, остальное как прежде
        $cartItem = $cart->add(18, 'tp', 1, 15.3, []);

        $this->assertCount(2, $cart->getItems());
        $this->assertSame(18, $cartItem->getId());
        $this->assertSame('tp', $cartItem->getTitle());
        $this->assertTrue($cartItem->getCount() == 3);
        $this->assertSame([], $cartItem->getOptions());

        // Buyable
        $model = new BuyableModelProduct();

        $cartItem2 = $cart->add($model, 3, ['some' => 'data']);

        $this->assertCount(3, $cart->getItems());

        $cartItem2 = $cart->add($model, 7, ['some' => 'data']);

        $cartItem3 = $cart->add($model, 9);

        $this->assertCount(4, $cart->getItems());
        $this->assertTrue($cartItem2->getCount() == 10);

        // добавление массив
        $prod = [
            'id' => 101,
            'title' => 'prodArr',
            'count' => 1,
            'price' => 6,
            'options' => [],
        ];

        $cartItem4 = $cart->add($prod);

        $this->assertCount(5, $cart->getItems());

        // добавление нескольких товаров одновременно
        $mProd = new BuyableModelProduct(200, 'multi', 10);
        $mProd1 = new BuyableModelProduct(201, 'multi1', 12);
        $mProd2 = [
            'id' => 202,
            'title' => 'multi2',
            'count' => 3,
            'price' => 6,
            'options' => [],
        ];

        // возвращает массив каждый элемент которого - экземпляр Item
        $cartItems = $cart->add([$mProd, $mProd1, $mProd2]);

        $this->assertCount(8, $cart->getItems());
        $this->assertCount(3, $cartItems);
    }

    /**
     * @expectedException \Mavsan\LaCart\Exceptions\CartInvalidRowIDException
     */
    public function testGetItemException()
    {
        $cart = $this->getCart();

        $cart->getItem('wrongRowId');
    }

    public function testGetItem()
    {
        $cart = $this->getCart();

        $prod = [
            'id' => 1,
            'title' => 'prodArr',
            'count' => 1,
            'price' => 6,
            'options' => [],
        ];

        $prod1 = [
            'id' => 11,
            'title' => 'prodArr11',
            'count' => 111,
            'price' => 611,
            'options' => [],
        ];

        $cartItem = $cart->add($prod);
        $cartItem1 = $cart->add($prod1);

        $prodCart = $cart->getItem($cartItem->getRowID());
        $prodCart1 = $cart->getItem($cartItem1->getRowID());

        $this->assertSame($prodCart->getID(), $prod['id']);
        $this->assertSame($prodCart1->getID(), $prod1['id']);
    }

    public function testUpdateItem()
    {
        $cart = $this->getCart();

        $model = [
            'id' => 101,
            'title' => 'prodArr',
            'count' => 4,
            'price' => 6,
            'options' => [],
        ];
        $item = $cart->add($model);

        $oldRowID = $item->getRowID();
        $itemCount = $item->getCount();
        $itemPrice = $item->getPrice();
        $newCount = 55;
        $newPrice = 999;

        $this->assertTrue($newCount != $itemCount);
        $this->assertTrue($itemPrice != $newPrice);
        $this->assertCount(1, $cart->getItems());

        $item->setCount(55);
        $item->setPrice($newPrice);
        $item->setId('newId');

        $this->expectsEvents('cart.updated');

        $cart->updateItem($oldRowID, $item);

        $cart = new Cart();
        $item2 = $cart->getItem($item->getRowID());

        $this->assertSame($newCount, $item2->getCount());
        $this->assertSame($newPrice, $item2->getPrice());
        $this->assertCount(1, $cart->getItems());

        // при обновлении заказа, если изменился rowID и такой rowID уже есть
        // в корзине - кол-во должно стать то, которое уже есть + то, которое в
        // измененной записи

        $cart = new Cart();
        $item2 = $cart->add($model);
        $itemCount = $item->getCount();
        $oldRowID = $item->getRowID();
        $item2Count = $item2->getCount();

        $this->assertCount(2, $cart->getItems());

        $item->setId($model['id']);
        $item = $cart->updateItem($oldRowID, $item);

        $this->assertCount(1, $cart->getItems());
        $this->assertSame($itemCount + $item2Count, $item->getCount());
    }

    public function testRemoveItem()
    {
        $cart = $this->getCart();
        $item = $cart->add(new BuyableModelProduct(1));
        $item1 = $cart->add(new BuyableModelProduct(2));
        $item2 = $cart->add(new BuyableModelProduct(3));

        $this->assertTrue($cart->getItems()->has($item->getRowID()));
        $this->assertTrue($cart->getItems()->has($item1->getRowID()));
        $this->assertTrue($cart->getItems()->has($item2->getRowID()));

        $this->expectsEvents('cart.removed');

        $cart->removeItem($item1->getRowID());
        $cart->removeItem($item2);

        $this->assertTrue($cart->getItems()->has($item->getRowID()));
        $this->assertFalse($cart->getItems()->has($item1->getRowID()));
        $this->assertFalse($cart->getItems()->has($item2->getRowID()));

        $cart = new Cart();

        $this->assertTrue($cart->getItems()->has($item->getRowID()));
        $this->assertFalse($cart->getItems()->has($item1->getRowID()));
        $this->assertFalse($cart->getItems()->has($item2->getRowID()));
    }

    public function testUpdateFromBuyable()
    {
        $cart = $this->getCart();

        $model = new BuyableModelProduct();
        $item = $cart->add($model);
        $oldItem = clone $item;

        $this->assertCount(1, $cart->getItems());

        $model = new BuyableModelProduct($model->cartGetID(), 'new title', 10000.00);

        $this->expectsEvents('cart.updated');

        $cart->update($item->getRowID(), $model);

        $this->assertCount(1, $cart->getItems());
        $this->assertNotSame($oldItem->getTitle(), $item->getTitle());
        $this->assertNotSame($oldItem->getPrice(), $item->getPrice());
        $this->assertSame($oldItem->getCount(), $item->getCount());

        $this->assertSame($model->cartGetTitle(), $item->getTitle());
        $this->assertSame($model->cartGetPrice(), $item->getPrice());

        $model = new BuyableModelProduct('testID', 'title 1', 100.00);
        $item2 = $cart->add($model);
        $item2->setCount(5);
        $cart->updateItem($item2->getRowID(), $item2);
        $oldItem = clone $item;
        $oldCount = $item2->getCount();
        $newCount = $item->getCount();

        $this->assertCount(2, $cart->getItems());

        $item = $cart->update($item->getRowID(), $model);

        $this->assertCount(1, $cart->getItems());

        $this->assertNotSame($oldItem->getTitle(), $item->getTitle());
        $this->assertNotSame($oldItem->getPrice(), $item->getPrice());
        $this->assertNotSame($oldItem->getCount(), $item->getCount());
        $this->assertTrue($oldCount + $newCount == $item->getCount());
    }

    public function testUpdateCount()
    {
        $cart = $this->getCart();

        $item = $cart->add(new BuyableModelProduct());

        $cart->update($item->getRowID(), 1010102);

        $cart = new Cart();
        $item = $cart->getItem($item->getRowID());

        $this->assertSame(1010102, $item->getCount());
    }

    public function testRemoveZeroCount()
    {
        $cart = $this->getCart();
        $model1 = [
            'id' => 100,
            'title' => 'prodArr',
            'count' => 0,
            'price' => 6.1,
            'options' => [],
        ];
        $model2 = new BuyableModelProduct(2);
        $model3 = [
            'id' => 101,
            'title' => 'prodArr1',
            'count' => 0,
            'price' => 6,
            'options' => [],
        ];

        $item1 = $cart->add($model1);
        $item2 = $cart->add($model2);
        $item3 = $cart->add($model3);
        $item4 = $cart->add(new BuyableModelProduct());

        // 2 - т.к. у двух позиций кол-во = 0
        $this->assertCount(2, $cart->getItems());

        $cart->update($item2->getRowID(), 0);

        // если установить кол-во = 0 - запись удалится автоматически
        $this->assertCount(1, $cart->getItems());
    }

    public function testUpdateFromArray()
    {
        $cart = $this->getCart();
        $model = new BuyableModelProduct(1);
        $model1 = [
            'id' => 101,
            'title' => 'prodArr',
            'count' => 1,
            'price' => 6,
            'options' => [],
        ];

        $item = $cart->add($model);
        $oldItem = clone $item;

        $this->assertCount(1, $cart->getItems());

        $this->expectsEvents('cart.updated');

        $item = $cart->update($item->getRowID(), $model1);

        $this->assertCount(1, $cart->getItems());
        $this->assertNotSame($oldItem->getTitle(), $item->getTitle());
        $this->assertNotSame($oldItem->getPrice(), $item->getPrice());
        $this->assertSame($oldItem->getCount(), $item->getCount());

        $this->assertSame($model1['title'], $item->getTitle());
        $this->assertSame($model1['price'], $item->getPrice());

        $model2 = [
            'id' => 102,
            'title' => 'prodArr2',
            'count' => 3,
            'price' => 12,
            'options' => [],
        ];
        $item2 = $cart->add($model2);
        $oldItem = clone $item;
        $oldCount = $item2->getCount();
        $newCount = $model2['count'];

        $this->assertCount(2, $cart->getItems());

        $item = $cart->update($item->getRowID(), $model2);

        $this->assertCount(1, $cart->getItems());
        $this->assertNotSame($oldItem->getTitle(), $item->getTitle());
        $this->assertNotSame($oldItem->getPrice(), $item->getPrice());
        $this->assertNotSame($oldItem->getCount(), $item->getCount());
        $this->assertTrue($oldCount + $newCount == $item->getCount());
    }

    public function testCount()
    {
        $cart = new Cart();

        $item1 = $cart->add(new BuyableModelProduct(1));
        $item2 = $cart->add(new BuyableModelProduct(2));
        $item3 = $cart->add(new BuyableModelProduct(3));
        $item4 = $cart->add(new BuyableModelProduct(4));

        $cart->update($item1->getRowID(), 3);
        $cart->update($item2->getRowID(), 5);
        $cart->update($item3->getRowID(), 7);
        $cart->update($item4->getRowID(), 10);

        $this->assertSame(25, $cart->count());
    }

    public function testClear()
    {
        $cart = new Cart();

        $cart->add(new BuyableModelProduct(1));
        $cart->add(new BuyableModelProduct(2));
        $cart->add(new BuyableModelProduct(3));
        $cart->add(new BuyableModelProduct(4));

        $this->assertCount(4, $cart->getItems());

        $cart->clear();

        $this->assertCount(0, $cart->getItems());
    }

    public function testTotalPrice()
    {
        $cart = new Cart();

        $item = $cart->add(new BuyableModelProduct(1, 't', 120.11));
        $item1 = $cart->add(new BuyableModelProduct(2, 't', 10));
        $item2 = $cart->add(new BuyableModelProduct(3, 't', 90.54));
        $item3 = $cart->add(new BuyableModelProduct(4, 't', 220.4));

        $cart->update($item->getRowID(), 3);
        $cart->update($item1->getRowID(), 5);
        $cart->update($item2->getRowID(), 1);
        $cart->update($item3->getRowID(), 14);

        $this->assertSame(3586.47, $cart->totalPrice());
        $this->assertSame('3-586|5', $cart->formatTotalPrice(1, '|', '-'));
    }

    public function testTotalDiscount()
    {
        $cart = new Cart();

        /** @var CartDiscount $discountRules */
        $discountRules = \App::make('cartDiscount');
        $discountRules->add(1000, 5, 50);
        $discountRules->add(2000, 7, 100);
        $discountRules->add(5000, 10, 150);
        $discountRules->add(10000, 15, 250);

        config(['cart.cartDiscount' => 'productsTotal,percent,discount']);

        $item = $cart->add(new BuyableModelProduct(1, 'pt', 200));
        $item1 = $cart->add(new BuyableModelProduct(2, 'pt', 500));
        $item2 = $cart->add(new BuyableModelProduct(3, 'pt', 100));

        // 200 + 500 + 100
        $this->assertSame(800, $cart->totalDiscount());

        $item3 = $cart->add(new BuyableModelProduct(4, 'pt', 2000));

        // (200 + 500 + 100 + 2000) - 7% - 100
        $this->assertSame(2504, $cart->totalDiscount());

        $item3->setCount(3);

        // (200 + 500 + 100 + 6000) - 10% - 150
        $this->assertSame(5970, $cart->totalDiscount());

        $item3->setCount(6);

        // (200 + 500 + 100 + 12000) - 15% - 250
        $this->assertSame(10630, $cart->totalDiscount());

        $cart->removeItem($item3->getRowID());
        config(['cart.cartDiscount' => 'productsTotal,discount,percent']);

        // 200 + 500 + 100
        $this->assertSame(800, $cart->totalDiscount());

        $item3 = $cart->add(new BuyableModelProduct(4, 'pt', 2000));

        // (200 + 500 + 100 + 2000) - 100 - 7%
        $this->assertSame(2511, $cart->totalDiscount());

        $item3->setCount(3);

        // (200 + 500 + 100 + 6000) - 150 - 10%
        $this->assertSame(5985, $cart->totalDiscount());

        $item3->setCount(6);

        // (200 + 500 + 100 + 12000) - 250 - 15%
        $this->assertSame(10667.5, $cart->totalDiscount());

        $item3->setDiscountPercent(4);
        $item3->setDiscount(12.18);
        config(['cart.cartDiscount' => 'percent,discount']);
        config(['cart.itemDiscount' => 'discount,percent']);

        // $item3 - стоимость (2000 - 12,18 - 4%) * 6 = 11449.8432
        // (200 + 500 + 100 + 11449.8432) = 12249.8432  - 15% - 250
        $this->assertSame(10162.36672, $cart->totalDiscount());

        $cart->setRound(true);

        $this->assertSame(10162.37, $cart->totalDiscount());

        $cart->setRound(false);

        // форматированный вывод
        $this->assertSame('10-162`37', $cart->formatTotalDiscount(2, '`', '-'));

        // отрицательные значения скидки - наценка
        $discountRules->add(15000, -10, -100);
        $item3->setCount(8);

        // $item3 - стоимость (2000 - 12,18 - 4%) * 8 = 15266,4576
        // (200 + 500 + 100 + 15266,4576) = 16066,4576  + 10% + 100
        $this->assertSame(17773.10336, $cart->totalDiscount());
    }

    public function testSetRound()
    {
        $cart = new Cart();

        $this->assertFalse($cart->isRound());

        $cart->setRound(true);

        $this->assertTrue($cart->isRound());

        $cart->setRound(false);

        $this->assertFalse($cart->isRound());

        $cart->setRound(1);

        $this->assertTrue($cart->isRound());

        $cart->setRound(false);

        $this->assertFalse($cart->isRound());

        $cart->setRound('1');

        $this->assertTrue($cart->isRound());

        $cart->setRound('true');

        $this->assertFalse($cart->isRound());
    }

    public function testClearOnLogout()
    {
        config(['cart.clearCartOnLogout' => true]);

        $cart = $this->getCart();
        $cart->add(new BuyableModelProduct(1));
        $cart->add(new BuyableModelProduct(2));

        $this->assertCount(2, $cart->getItems());

        \Event::fire('Illuminate\Auth\Events\Logout');

        $cart = $this->getCart();

        $this->assertCount(0, $cart->getItems());
    }

    public function testCallableIniCartDiscountRules()
    {
        $cart = new Cart();
        /** @var CartDiscount $cd */
        $cd = \App::make('cartDiscount');

        $this->assertCount(0, $cd->getItems());

        // статический метод
        config(['cart.cartDiscountRules' => 'Mavsan\LaCart\test\Fixtures\CartDiscount::one']);
        $cart->totalDiscount();

        $this->assertCount(1, $cd->getItems());
        $this->assertTrue($cd->getItems()->has(10));
        /** @var \Mavsan\LaCart\Models\CartDiscountItem $rule */
        $rule = $cd->getItems()->get(10);
        $this->assertSame(10, $rule->getSumFrom());
        $this->assertSame(11, $rule->getPercent());
        $this->assertSame(12, $rule->getDiscount());

        // обычный метод
        config(['cart.cartDiscountRules' => 'Mavsan\LaCart\test\Fixtures\CartDiscount::two']);
        $cart->totalDiscount();

        $this->assertCount(2, $cd->getItems());
        $this->assertTrue($cd->getItems()->has(20));
        $rule = $cd->getItems()->get(20);
        $this->assertSame(20, $rule->getSumFrom());
        $this->assertSame(21, $rule->getPercent());
        $this->assertSame(22, $rule->getDiscount());

        // обычный метод
        config(['cart.cartDiscountRules' => 'Mavsan\LaCart\test\Fixtures\CartDiscount@three']);
        $cart->totalDiscount();

        $this->assertCount(3, $cd->getItems());
        $this->assertTrue($cd->getItems()->has(30));
        $rule = $cd->getItems()->get(30);
        $this->assertSame(30, $rule->getSumFrom());
        $this->assertSame(31, $rule->getPercent());
        $this->assertSame(32, $rule->getDiscount());

        config([
            'cart.cartDiscountRules' => function () {
                \App::make('cartDiscount')->add(40, 41, 42);
            },
        ]);
        $cart->totalDiscount();

        $this->assertCount(4, $cd->getItems());
        $this->assertTrue($cd->getItems()->has(40));
        $rule = $cd->getItems()->get(40);
        $this->assertSame(40, $rule->getSumFrom());
        $this->assertSame(41, $rule->getPercent());
        $this->assertSame(42, $rule->getDiscount());

        require_once __DIR__.'/../Fixtures/functions.php';

        config(['cart.cartDiscountRules' => 'testCartDiscountCallable']);
        $cart->totalDiscount();

        $this->assertCount(5, $cd->getItems());
        $this->assertTrue($cd->getItems()->has(50));
        $rule = $cd->getItems()->get(50);
        $this->assertSame(50, $rule->getSumFrom());
        $this->assertSame(51, $rule->getPercent());
        $this->assertSame(52, $rule->getDiscount());
    }

    public function testSearch()
    {
        $cart = $this->getCart();

        $item = $cart->add(1, 'Product1', 3, 20, ['color' => 'yellow']);
        $item1 = $cart->add(2, 'Product2', 5, 30, ['color' => 'white']);
        $item2 = $cart->add(3, 'Product3', 2, 5, ['color' => 'white']);

        $f = $cart->search(function ($item, $index) {
            /** @var \Mavsan\LaCart\Models\CartItem $item */
            return $item->getPrice() > 10;
        });

        $this->assertCount(2, $f);
        $this->assertTrue($f->has($item->getRowID()));
        $this->assertTrue($f->has($item1->getRowID()));

        $f = $cart->search(function ($item, $index) {
            /** @var \Mavsan\LaCart\Models\CartItem $item */
            return $item->getOptions()['color'] == 'white';
        });

        $this->assertCount(2, $f);
        $this->assertTrue($f->has($item1->getRowID()));
        $this->assertTrue($f->has($item2->getRowID()));
    }

    public function testSetModel()
    {
        $cart = $this->getCart();

        $item = $cart->add(1, 'Product1', 3, 20);

        $this->assertEmpty($item->getModel());

        $cart->setModel($item->getRowID(), ModelProduct::class);

        $this->assertSame(ModelProduct::class, $item->getModel());
        $this->assertInstanceOf(ModelProduct::class, $item->getModel(true));

        $this->expectException(CartItemModelNotFoundException::class);

        $cart->setModel($item->getRowID(), 'wrong/model');
    }

    public function testGetModel()
    {
        $cart = $this->getCart();
        $item = $cart->add(1, 'Product1', 3, 20);

        $this->assertEmpty($item->getModel());

        $cart->setModel($item->getRowID(), ModelProduct::class);

        $this->assertSame(ModelProduct::class, $cart->getModel($item->getRowID()));
        $this->assertInstanceOf(ModelProduct::class, $cart->getModel($item->getRowID(), true));

        $cart->clear();
        $item = $cart->add(1, 'Product1', 3, 20);

        $this->expectException(CartItemModelNotFoundException::class);

        $cart->getModel($item->getRowID(), true);
    }

    public function testIsChanged()
    {
        $cartReflection = new \ReflectionClass('Mavsan\LaCart\Models\Cart');
        $changedProperty = $cartReflection->getProperty('changed');
        $changedProperty->setAccessible(true);

        $cartItemsProperty = $cartReflection->getProperty('cartItems');
        $cartItemsProperty->setAccessible(true);

        $cart = new Cart();

        // changed после добавления
        $this->assertFalse($cart->isChanged());
        $item = $cart->add(5, 'title', 2, 15);
        $this->assertTrue($cart->isChanged());

        $changedProperty->setValue($cart, false);
        $this->assertFalse($cart->isChanged());

        $item->setTitle('newTitle');
        // changed после обновления
        $cart->updateItem($item->getRowID(), $item);

        $this->assertTrue($cart->isChanged());
        $changedProperty->setValue($cart, false);
        $this->assertFalse($cart->isChanged());

        // changed после обновления 2
        $cart->update($item->getRowID(), 15);

        $this->assertTrue($cart->isChanged());

        $item2 = $cart->add('item2ID', 'titleItem2', 5, 7);

        $changedProperty->setValue($cart, false);
        $this->assertFalse($cart->isChanged());

        // changed после удаления
        $cart->removeItem($item2->getRowID());

        $this->assertTrue($cart->isChanged());

        // проверка changed при очистке корзины от товаров, у которых кол-во = 0
        $item2 = $cart->add('item2ID', 'titleItem2', 5, 7);
        $item2->setCount(0);
        /** @var \Illuminate\Support\Collection $cartItems */
        $cartItems = $cartItemsProperty->getValue($cart);
        $cartItems[$item2->getRowID()] = $item2;
        $cartItemsProperty->setValue($cart, $cartItems);

        $changedProperty->setValue($cart, false);
        $this->assertFalse($cart->isChanged());

        // здесь произойдет удаление товара с кол-вом = 0
        $cart->getItems();

        $this->assertTrue($cart->isChanged());
        $changedProperty->setValue($cart, false);
        $this->assertFalse($cart->isChanged());

        // changed при очистке корзины
        $cart->clear();

        $this->assertTrue($cart->isChanged());
    }
}