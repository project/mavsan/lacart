<?php
/**
 * CartDiscountTest.php
 * Date: 03.08.2017
 * Time: 17:59
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\test\Unit;


use Mavsan\LaCart\Models\CartDiscount;
use Tests\TestCase;

class CartDiscountTest extends TestCase
{

    public function testGetItems()
    {
        $discountRules = new CartDiscount();

        $o = [
            ['f' => 100, 'p' => 10, 'd' => 10,],
            ['f' => 500, 'p' => 20, 'd' => 100,],
            ['f' => 1000, 'p' => 40, 'd' => 400,],
            ['f' => 10000, 'p' => 60, 'd' => 600,],
        ];

        $discountRules->add($o[1]['f'], $o[1]['p'], $o[1]['d']);
        $discountRules->add($o[3]['f'], $o[3]['p'], $o[3]['d']);
        $discountRules->add($o[2]['f'], $o[2]['p'], $o[2]['d']);
        $discountRules->add($o[0]['f'], $o[0]['p'], $o[0]['d']);

        $i = 0;

        // должно быть отсортировано по возрастанию "скидка от"
        /** @var \Mavsan\LaCart\Models\CartDiscountItem $rule */
        foreach ($discountRules as $idx => $rule) {
            $this->assertSame($o[$i], $idx);
            $this->assertSame($o[$i]['f'], $rule->getSumFrom());
            $this->assertSame($o[$i]['p'], $rule->getPercent());
            $this->assertSame($o[$i]['d'], $rule->getDiscount());
        }
    }

    public function testGetDiscountRule()
    {
        $discountRules = new CartDiscount();

        $o = [
            ['f' => 100, 'p' => 10, 'd' => 10,],
            ['f' => 500, 'p' => 20, 'd' => 100,],
            ['f' => 1000, 'p' => 40, 'd' => 400,],
            ['f' => 10000, 'p' => 60, 'd' => 600,],
        ];

        $discountRules->add($o[1]['f'], $o[1]['p'], $o[1]['d']);
        $discountRules->add($o[3]['f'], $o[3]['p'], $o[3]['d']);
        $discountRules->add($o[2]['f'], $o[2]['p'], $o[2]['d']);
        $discountRules->add($o[0]['f'], $o[0]['p'], $o[0]['d']);

        /** @var \Mavsan\LaCart\Models\CartDiscountItem $rule */
        $rule = $discountRules->getDiscountRule(25);
        $this->assertSame(0, $rule->getSumFrom());
        $this->assertSame(0, $rule->getPercent());
        $this->assertSame(0, $rule->getDiscount());

        $rule = $discountRules->getDiscountRule(505);
        $this->assertSame(500, $rule->getSumFrom());
        $this->assertSame(20, $rule->getPercent());
        $this->assertSame(100, $rule->getDiscount());

        $rule = $discountRules->getDiscountRule(10001);
        $this->assertSame(10000, $rule->getSumFrom());
        $this->assertSame(60, $rule->getPercent());
        $this->assertSame(600, $rule->getDiscount());

        $rule = $discountRules->getDiscountRule(105);
        $this->assertSame(100, $rule->getSumFrom());
        $this->assertSame(10, $rule->getPercent());
        $this->assertSame(10, $rule->getDiscount());

        $rule = $discountRules->getDiscountRule(1005);
        $this->assertSame(1000, $rule->getSumFrom());
        $this->assertSame(40, $rule->getPercent());
        $this->assertSame(400, $rule->getDiscount());
    }


}