<?php
/**
 * CartRepositoryTest.php
 * Date: 02.08.2017
 * Time: 11:51
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\test\Unit;


use Mavsan\LaCart\Helper\CartHelper;
use Tests\TestCase;

class CartHelperTest extends TestCase
{

    public function testNumberFormat()
    {
        config([
            'cart.format.decimals'      => '3',
            'cart.format.dec_point'     => '||',
            'cart.format.thousands_sep' => '&nbsp;',
        ]);

        $this->assertSame('123&nbsp;456&nbsp;789||123',
            CartHelper::number_format(123456789.123456));

        $this->assertSame('123`456`789,12',
            CartHelper::number_format(123456789.123456, 2, ',', '`'));
    }
}