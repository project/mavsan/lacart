<?php
/**
 * CartDBTest.php
 * Date: 04.08.2017
 * Time: 12:42
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\test\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mavsan\LaCart\Exceptions\CartAlreadyStoredException;
use Mavsan\LaCart\Models\Cart;
use Tests\TestCase;

class CartDBTest extends TestCase
{
    use DatabaseMigrations;

    public function testStore()
    {
        $cart = new Cart();

        $cart->add(1, 'Product', 8, 14.2, ['type' => 'H']);
        $cart->add(10, 'Product 10', 2, 10.15, ['mode' => 'Y', 'ver' => 12]);

        $this->assertDatabaseMissing('cart', ['identifier' => 'myIdentifier']);

        $this->expectsEvents('cart.stored');

        $cart->store('myIdentifier');

        $this->assertDatabaseHas('cart', ['identifier' => 'myIdentifier']);

        $this->expectException(CartAlreadyStoredException::class);
        $this->expectExceptionMessage(__('cart::cart.cart.storeError'));

        $cart->store('myIdentifier');
    }

    public function testStoreUpdate()
    {
        $cart = new Cart();

        $cart->add(1, 'Product', 8, 14.2, ['type' => 'H']);
        $cart->add(10, 'Product 10', 2, 10.15, ['mode' => 'Y', 'ver' => 12]);

        $this->assertDatabaseMissing('cart', ['identifier' => 'myIdentifier']);

        $this->expectsEvents('cart.stored');

        // в базе нет такой записи, добавление
        $cart->store('myIdentifier', true);

        $this->assertDatabaseHas('cart', ['identifier' => 'myIdentifier']);

        // в базе есть такая запись, обновление
        $cart->store('myIdentifier', true);

        $this->assertDatabaseHas('cart', ['identifier' => 'myIdentifier']);
    }

    public function testCheckCartSavedInDB()
    {
        $cart = new Cart();

        $this->assertFalse($cart->isCartStoredInDB('someID'));

        $cart->add(1, 'Product', 8, 14.2, ['type' => 'H']);
        $cart->add(10, 'Product 10', 2, 10.15, ['mode' => 'Y', 'ver' => 12]);

        $cart->store('someID');

        $this->assertTrue($cart->isCartStoredInDB('someID'));
    }

    public function cartDeleteFromDB()
    {
        $cart = new Cart();

        $this->assertDatabaseMissing('cart', ['identifier' => 'someID']);

        $cart->add(1, 'Product', 8, 14.2, ['type' => 'H']);
        $cart->add(10, 'Product 10', 2, 10.15, ['mode' => 'Y', 'ver' => 12]);

        $cart->store('someID');

        $this->assertDatabaseHas('cart', ['identifier' => 'someID']);
        $this->expectsEvents('cart.deletedFromDB');

        $cart->deleteCartFromDB('someID');

        $this->assertDatabaseMissing('cart', ['identifier' => 'someID']);
    }

    public function testRestore()
    {
        $cart = new Cart();

        $item1 = $cart->add(1, 'Product', 8, 14.2, ['type' => 'H']);
        $item2 = $cart->add(2, 'Product', 4, 10.55, ['type' => 'G']);

        $cart->store('myIdentifier');

        $item3 = $cart->add(10, 'Product 10', 2, 10.15, ['mode' => 'Y', 'ver' => 12]);

        $this->assertDatabaseHas('cart', ['identifier' => 'myIdentifier']);

        $this->expectsEvents('cart.restored');

        $cart->restore('myIdentifier');
        $item1 = $cart->getItem($item1->getRowID());
        $item2 = $cart->getItem($item2->getRowID());

        $cart->deleteCartFromDB('myIdentifier');

        $this->assertDatabaseMissing('cart', ['identifier' => 'myIdentifier']);
        $this->assertCount(3, $cart->getItems());
        $this->assertSame(8, $item1->getCount());
        $this->assertSame(4, $item2->getCount());
        $this->assertSame(2, $item3->getCount());

        $cart->store('myIdentifier');
        $cart->clear();
        $cart->restore('myIdentifier');

        $this->assertCount(3, $cart->getItems());
        $this->assertSame(8, $item1->getCount());
        $this->assertSame(4, $item2->getCount());
        $this->assertSame(2, $item3->getCount());
    }
}