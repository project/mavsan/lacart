<?php
/**
 * ItemTest.php
 * Date: 01.08.2017
 * Time: 11:15
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\test\Unit;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;
use Mavsan\LaCart\Exceptions\CartItemModelNotFoundException;
use Mavsan\LaCart\Models\CartItem;
use Mavsan\LaCart\test\Fixtures\BuyableModelProduct;
use Mavsan\LaCart\test\Fixtures\ModelProduct;
use Tests\TestCase;

class CartItemTest extends TestCase
{
    public function testImplements()
    {
        $item = new CartItem(12, 'ProductTitle', 123.14, 2, ['size' => 'L']);

        $this->assertTrue($item instanceof Arrayable);
        $this->assertTrue($item instanceof Jsonable);
    }

    public function testRowID()
    {
        $item = new CartItem(12, 'ProductTitle', 123.14, 2, ['size' => 'L']);

        $rowID = md5('12'.serialize(['size' => 'L']));

        $this->assertEquals($rowID, $item->getRowID());

        $item = new CartItem(8, 'ProductTitle', 123.14, 2, ['size' => 'L']);

        $rowID = md5('8'.serialize(['size' => 'L']));

        $this->assertEquals($rowID, $item->getRowID());

        $item = new CartItem(8, 'ProductTitle', 123.14, 2, ['color' => 'black']);

        $rowID = md5('8'.serialize(['color' => 'black']));

        $this->assertEquals($rowID, $item->getRowID());
    }

    public function testCreateFromBuyable()
    {
        $product = new BuyableModelProduct();

        $item = CartItem::createFromBuyable($product, 8, ['beer' => 'or not']);

        $this->assertSame($product->cartGetID(), $item->getId());
        $this->assertSame($product->cartGetPrice(), $item->getPrice());
        $this->assertSame($product->cartGetTitle(), $item->getTitle());
        $this->assertSame(8, $item->getCount());
        $this->assertSame(['beer' => 'or not'], $item->getOptions());
    }

    public function testCreateFromArray()
    {
        $data = [
            'id' => 17,
            'title' => 'pp',
            'count' => 5,
            'price' => 15.5,
            'options' => ['some' => 'there'],
        ];

        $item = CartItem::createFromArray($data);

        $this->assertSame(Arr::get($data, 'id'), $item->getId());
        $this->assertSame(Arr::get($data, 'price'), $item->getPrice());
        $this->assertSame(Arr::get($data, 'title'), $item->getTitle());
        $this->assertSame(Arr::get($data, 'count'), $item->getCount());
        $this->assertSame(Arr::get($data, 'options'), $item->getOptions());
    }

    public function testCreate()
    {
        $id = 1;
        $title = 'ddd';
        $count = 7;
        $price = 22;
        $options = ['opt' => 'val'];

        $item = new CartItem($id, $title, $price, $count, $options);

        $this->assertSame($id, $item->getId());
        $this->assertSame($title, $item->getTitle());
        $this->assertSame($count, $item->getCount());
        $this->assertSame($price, $item->getPrice());
        $this->assertSame($options, $item->getOptions());
    }

    public function testSetModel()
    {
        $item = $this->getItem();

        $this->expectsEvents('cart.itemUpdated');

        $this->assertEmpty($item->getModel());

        $item->setModel(BuyableModelProduct::class);

        $this->assertSame(BuyableModelProduct::class, $item->getModel());

        $item->setModel(new ModelProduct());

        $this->assertSame(ModelProduct::class, $item->getModel());

        $this->expectException(CartItemModelNotFoundException::class);

        $item->setModel('');

        $item->setModel(12);
    }

    protected function getItem(
        $id = 12,
        $title = 'ProductTitle',
        $price = 123.45,
        $count = 2,
        $options = ['size' => 'L']
    ) {
        return new CartItem($id, $title, $price, $count, $options);
    }

    public function testUpdateFromBuyable()
    {
        $item = $this->getItem();
        $oldItem = clone $item;
        $model = new BuyableModelProduct('buyable-999', 'updBuyable', 100000.00);

        $this->expectsEvents('cart.itemUpdated');

        $item->updateFromBuyable($model);

        $this->assertNotSame($oldItem->getRowID(), $item->getRowID());
        $this->assertNotSame($oldItem->getTitle(), $item->getTitle());
        $this->assertNotSame($oldItem->getPrice(), $item->getPrice());
        $this->assertNotSame($oldItem->getId(), $item->getId());

        $this->assertSame('updBuyable', $item->getTitle());
        $this->assertSame(100000.00, $item->getPrice());
    }

    public function testUpdateFromArray()
    {
        $item = $this->getItem();
        $oldItem = clone $item;
        $data = [
            'id' => 17,
            'title' => 'pp',
            'count' => 5,
            'price' => 15.5,
            'options' => ['some' => 'there'],
        ];

        $this->expectsEvents('cart.itemUpdated');

        $item->updateFromArray($data);

        $this->assertNotSame($oldItem->getRowID(), $item->getRowID());
        $this->assertNotSame($oldItem->getTitle(), $item->getTitle());
        $this->assertNotSame($oldItem->getPrice(), $item->getPrice());
        $this->assertNotSame($oldItem->getId(), $item->getId());
        $this->assertNotSame($oldItem->getCount(), $item->getCount());
        $this->assertNotSame($oldItem->getOptions(), $item->getOptions());

        $this->assertSame($data['id'], $item->getId());
        $this->assertSame($data['title'], $item->getTitle());
        $this->assertSame($data['count'], $item->getCount());
        $this->assertSame($data['price'], $item->getPrice());
        $this->assertSame($data['options'], $item->getOptions());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSetCount()
    {
        $this->expectsEvents('cart.itemUpdated');

        $item = $this->getItem();
        $oldCount = $item->getCount();
        $newCount = $oldCount + 10;
        $item->setCount($newCount);

        $this->assertNotSame($oldCount, $item->getCount());
        $this->assertSame($newCount, $item->getCount());

        $this->expectExceptionMessage(__('cart::cart.cartItem.invalidCountValue'));

        $item->setCount('one');
    }

    public function testGetTotalPrice()
    {
        $item = $this->getItem();
        $item->setPrice(252.14);
        $item->setCount(8);

        $this->assertSame(2017.12, $item->getTotalPrice());

        $this->assertSame('2 017,12', $item->formatTotalPrice(2, ',', ' '));
    }

    public function testGetModel()
    {
        $item = $this->getItem();

        $this->assertEmpty($item->getModel());

        $item->setModel(BuyableModelProduct::class);

        $this->assertSame(BuyableModelProduct::class, $item->getModel());
        $this->assertInstanceOf(BuyableModelProduct::class, $item->getModel(true));

        $this->expectException(CartItemModelNotFoundException::class);
        $this->expectExceptionMessage(__('cart::cart.cartItem.modelNotSet'));

        $item = $this->getItem();
        $item->getModel(true);
    }

    public function testGetPriceDiscount()
    {
        $item = $this->getItem();
        $item->setPrice(1499.99);
        $item->setDiscountPercent(10);
        $item->setDiscount(15.89);

        config(['cart.itemDiscount' => 'percent']);

        $this->assertSame(1349.991, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'discount']);

        $this->assertSame(1484.1, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'percent,discount']);

        $this->assertSame(1334.101, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'discount,percent']);

        $this->assertSame(1335.69, $item->getPriceDiscount());

        $item->setRound(true);

        config([
            'cart.itemDiscount' => 'percent',
            'cart.priceRound.precision' => 2,
            'cart.priceRound.mode' => PHP_ROUND_HALF_UP,
        ]);

        $this->assertSame(1349.99, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'discount']);

        $this->assertSame(1484.1, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'percent,discount']);

        $this->assertSame(1334.1, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'discount,percent']);

        $this->assertSame(1335.69, $item->getPriceDiscount());

        $this->assertSame('1 335|69', $item->formatPriceDiscount(2, '|', ' '));

        // отрицательные значения скидок
        $item->setDiscountPercent(-10);
        $item->setDiscount(-15.89);

        config(['cart.itemDiscount' => 'percent']);

        $this->assertSame(1649.99, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'discount']);

        $this->assertSame(1515.88, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'percent,discount']);

        $this->assertSame(1665.88, $item->getPriceDiscount());

        config(['cart.itemDiscount' => 'discount,percent']);

        $this->assertSame(1667.47, $item->getPriceDiscount());
    }

    public function testSetRound()
    {
        $this->expectsEvents('cart.itemUpdated');

        $item = $this->getItem();

        $this->assertFalse($item->isRound());

        $item->setRound(true);

        $this->assertTrue($item->isRound());

        $item->setRound(false);

        $this->assertFalse($item->isRound());

        $item->setRound(1);

        $this->assertTrue($item->isRound());

        $item->setRound(false);

        $this->assertFalse($item->isRound());

        $item->setRound('1');

        $this->assertTrue($item->isRound());

        $item->setRound('true');

        $this->assertFalse($item->isRound());
    }

    public function testGetTotalDiscount()
    {
        $item = $this->getItem();
        $item->setPrice(999.99);
        $item->setDiscountPercent(11);
        $item->setDiscount(9.89);
        $item->setCount(3);

        config(['cart.itemDiscount' => 'percent']);

        $this->assertSame(2669.9733, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'discount']);

        $this->assertSame(2970.3, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'percent,discount']);

        $this->assertSame(2640.3033, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'discount,percent']);

        $this->assertSame(2643.567, $item->getTotalDiscount());

        $item->setRound(true);

        config([
            'cart.itemDiscount' => 'percent',
            'cart.priceRound.precision' => 2,
            'cart.priceRound.mode' => PHP_ROUND_HALF_UP,
        ]);

        $this->assertSame(2669.97, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'discount']);

        $this->assertSame(2970.3, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'percent,discount']);

        $this->assertSame(2640.3, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'discount,percent']);

        $this->assertSame(2643.57, $item->getTotalDiscount());

        $this->assertSame('2+643-57', $item->formatTotalDiscount(2, '-', '+'));

        // отрицательные значения скидок
        $item->setDiscountPercent(-9);
        $item->setDiscount(-5.12);

        config(['cart.itemDiscount' => 'percent']);

        $this->assertSame(3269.97, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'discount']);

        $this->assertSame(3015.33, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'percent,discount']);

        $this->assertSame(3285.33, $item->getTotalDiscount());

        config(['cart.itemDiscount' => 'discount,percent']);

        $this->assertSame(3286.71, $item->getTotalDiscount());

        // настройки персональных скидок пустые
        config(['cart.itemDiscount' => '']);

        $this->assertSame($item->getTotalPrice(), $item->getTotalDiscount());
    }

    public function testSetID()
    {
        $this->expectsEvents('cart.itemUpdated');

        $item = $this->getItem();

        $oldRowID = $item->getRowID();

        $item->setId('sadas');

        $this->assertNotSame($oldRowID, $item->getRowID());
    }

    public function testSetTitle()
    {
        $this->expectsEvents('cart.itemUpdated');
        $item = $this->getItem();
        $item->setTitle('sadas');
    }

    public function testSetPrice()
    {
        $this->expectsEvents('cart.itemUpdated');
        $item = $this->getItem();
        $item->setPrice(159);
    }

    public function testSetOptions()
    {
        $this->expectsEvents('cart.itemUpdated');

        $item = $this->getItem();

        $oldRowID = $item->getRowID();

        $item->setOptions(['o' => 'o1', 'p' => 'p1']);

        $this->assertNotSame($oldRowID, $item->getRowID());
    }

    public function testSetDiscount()
    {
        $this->expectsEvents('cart.itemUpdated');
        $item = $this->getItem();
        $item->setDiscount(5);
    }

    public function testSetDiscountPercent()
    {
        $this->expectsEvents('cart.itemUpdated');
        $item = $this->getItem();
        $item->setDiscountPercent(3);
    }
}