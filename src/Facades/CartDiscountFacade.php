<?php
/**
 * CartDiscountFacade.php
 * Date: 03.08.2017
 * Time: 16:36
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Facades;

use Illuminate\Support\Facades\Facade;

class CartDiscountFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor()
    {
        return 'cartDiscount';
    }
}
