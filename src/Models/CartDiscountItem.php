<?php
/**
 * CartDiscountItem.php
 * Date: 03.08.2017
 * Time: 16:14
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Models;

class CartDiscountItem
{
    protected $sumFrom;

    protected $percent;

    protected $discount;

    /**
     * CartDiscountItem constructor.
     *
     * @param $sumFrom
     * @param $percent
     * @param $discount
     */
    public function __construct($sumFrom, $percent, $discount)
    {
        $this->sumFrom = $sumFrom;
        $this->percent = $percent;
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getSumFrom()
    {
        return $this->sumFrom;
    }

    /**
     * @return mixed
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
