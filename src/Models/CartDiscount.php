<?php
/**
 * CartDiscount.php
 * Date: 03.08.2017
 * Time: 16:08
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Models;

use Illuminate\Support\Collection;

class CartDiscount
{
    protected $items;

    /**
     * CartDiscount constructor.
     */
    public function __construct()
    {
        $this->items = new Collection();
    }

    /**
     * Добавить правило скидок
     *
     * @param float $sumFrom  сумма до
     * @param float $percent  процент скидки
     * @param float $discount фиксированная сумма скидки
     */
    public function add($sumFrom, $percent, $discount)
    {
        $this->items->put(
            $sumFrom,
            new CartDiscountItem($sumFrom, $percent, $discount)
        );
    }

    public function getDiscountRule($sum)
    {
        $discount = new CartDiscountItem(0, 0, 0);

        foreach ($this->getItems() as $item) {
            /** @var \Mavsan\LaCart\Models\CartDiscountItem $item */
            if ($sum >= $item->getSumFrom()) {
                $discount = $item;
            }
        }

        return $discount;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getItems()
    {
        return $this->items->sortBy(function ($item) {
            /** @var \Mavsan\LaCart\Models\CartDiscountItem $item */
            return $item->getSumFrom();
        });
    }
}
