<?php
/**
 * Cart.php
 * Date: 01.08.2017
 * Time: 9:14
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Models;

use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Mavsan\LaCart\Exceptions\CartAlreadyStoredException;
use Mavsan\LaCart\Exceptions\CartInvalidRowIDException;
use Mavsan\LaCart\Helper\CartHelper;
use Mavsan\LaCart\Interfaces\Buyable;

class Cart
{
    /** @var  \Illuminate\Support\Collection */
    protected $cartItems;

    /** @var bool округлять или нет сумму всего */
    protected $round = false;

    /** @var bool были ли изменения в корзине */
    protected $changed = false;

    public function __construct()
    {

        // на случай, если был обновлен элемент корзины (по-ссылке), но не был вызван метод корзины
        // Cart::updateItem(...) добавлен слушатель события элемента корзины
        \Event::listen('cart.itemUpdated', function () {
            $this->changed = true;
        });
    }

    /**
     * Добавление товара в корзину.
     *
     * В случае, если добавление производится экземпляром модели, реализующем
     * интерфейс Mavsan\LaCart\Interfaces\Buyable, то первым параметром ($id)
     * должна идти эта модель, затем ($title) - кол-во, затем ($count) - опции
     * товара, если нужны.
     * @see \Mavsan\LaCart\Models\CartItem::createFromBuyable()
     *
     * В случае, если добавление производится массивом - должен передаваться
     * единственный аргумент ($id) - массив, содержащий вне необходимые данные
     * ['id' => , 'title' => , 'count' => , 'price' => , 'options' => ]
     * @see \Mavsan\LaCart\Models\CartItem::createFromArray()
     *
     * @param mixed $id      ИД товара
     * @param mixed $title   Наименование товара
     * @param mixed $count   Количество
     * @param mixed $price   Стоимость
     * @param array $options Опции товара
     *
     * @return \Mavsan\LaCart\Models\CartItem
     */
    public function add(
        $id,
        $title = null,
        $count = null,
        $price = null,
        array $options = []
    ) {
        if ($this->isMultiAdding($id)) {
            return collect($id)->map(function ($item, $id) {
                return $this->add($item);
            });
        }

        $cartItem =
            $this->createCartItem($id, $title, $count, $price, $options);

        if ($this->getItems()->has($cartItem->getRowID())) {
            $cartItem->setCount($cartItem->getCount()
                                + $this->getItems()->get($cartItem->getRowID())
                                       ->getCount());
        }

        $this->getItems()->put($cartItem->getRowID(), $cartItem);

        $this->setItems();

        event('cart.added', $cartItem);

        $this->changed = true;

        return $cartItem;
    }

    /**
     * Переданы ли несколько товаров для добавления
     *
     * @param $item
     *
     * @return bool
     */
    private function isMultiAdding($item)
    {
        if (! is_array($item)) {
            return false;
        }

        return is_array(head($item)) || head($item) instanceof Buyable;
    }

    /**
     * Создание элемента корзины
     *
     * @param int|string $id
     * @param string     $title
     * @param int        $count
     * @param float      $price
     * @param array      $options
     *
     * @return \Mavsan\LaCart\Models\CartItem
     */
    protected function createCartItem($id, $title, $count, $price, $options)
    {
        if ($id instanceof Buyable) {
            // title - здесь кол-во товаров
            // count - опции товара
            $cartItem =
                CartItem::createFromBuyable($id, $title ?: 1, $count ?: []);
            $cartItem->setModel($id);
        } else if (is_array($id)) {
            $cartItem = CartItem::createFromArray($id);
        } else {
            $cartItem = new CartItem($id, $title, $price, $count, $options);
        }

        return $cartItem;
    }

    /**
     * Получение данных корзины
     *
     * @return \Illuminate\Support\Collection
     */
    public function getItems()
    {
        if (empty($this->cartItems)
            && ! $this->cartItems instanceof Collection) {
            $this->cartItems =
                Session::get(config('cart.instanceSessionName'),
                    new Collection());
        }

        $this->removeZeroCount();

        return $this->cartItems;
    }

    /**
     * Удаление позиций, кол-во у которых = 0
     */
    protected function removeZeroCount()
    {
        $this->cartItems->filter(function ($item, $idx) {
            /** @var \Mavsan\LaCart\Models\CartItem $item */
            return $item->getCount() <= 0;
        })->map(function ($item, $idx) {
            $this->cartItems->pull($idx);
            $this->changed = true;
        });
    }

    /**
     * Сохранение данных корзины
     */
    protected function setItems()
    {
        Session::put(config('cart.instanceSessionName'), $this->getItems());
    }

    /**
     * Обновление данных товара в корзине.
     *
     * @param string $rowID идентификатор строки
     * @param mixed  $data  Buyable или array или новое кол-во
     *
     * @return \Mavsan\LaCart\Models\CartItem
     */
    public function update($rowID, $data)
    {
        $cartItem = $this->getItem($rowID);

        if ($data instanceof Buyable) {
            $cartItem->updateFromBuyable($data);
        } else if (is_array($data)) {
            $cartItem->updateFromArray($data);
        } else {
            $cartItem->setCount($data);
        }

        $this->changed = true;

        return $this->updateItem($rowID, $cartItem);
    }

    /**
     * @param $itemID
     *
     * @return \Mavsan\LaCart\Models\CartItem
     * @throws \Mavsan\LaCart\Exceptions\CartInvalidRowIDException
     */
    public function getItem($itemID)
    {
        if (! $this->getItems()->has($itemID)) {
            throw new CartInvalidRowIDException();
        }

        return $this->getItems()->get($itemID);
    }

    /**
     * Обновление элемента корзины
     *
     * @param string                         $oldRowID старый rowID
     *                                                 обновляемого элемента
     *                                                 корзины
     * @param \Mavsan\LaCart\Models\CartItem $cartItem
     *
     * @return \Mavsan\LaCart\Models\CartItem
     */
    public function updateItem($oldRowID, CartItem $cartItem)
    {
        if ($oldRowID != $cartItem->getRowID()) {
            $this->removeItem($oldRowID);

            if ($this->getItems()->has($cartItem->getRowID())) {
                $existing = $this->getItems()->get($cartItem->getRowID());
                $cartItem->setCount($cartItem->getCount()
                                    + $existing->getCount());
            }
        }

        if ($cartItem->getCount() <= 0) {
            // удалится автоматически, removeItem нельзя использовать, т.к.
            // возникнет исключение - запись не найдена
            $this->getItems();

            return;
        }

        $this->getItems()->put($cartItem->getRowID(), $cartItem);

        $this->setItems();

        event('cart.updated', $cartItem);

        $this->changed = true;

        return $cartItem;
    }

    /**
     * Удаление записи из корзины
     *
     * @param string|\Mavsan\LaCart\Models\CartItem $rowID
     *
     * @return $this
     */
    public function removeItem($rowID)
    {
        if ($rowID instanceof CartItem) {
            $rowID = $rowID->getRowID();
        }

        $cartItem = $this->getItem($rowID);

        $this->getItems()->pull($rowID);

        $this->setItems();

        event('cart.removed', $cartItem);

        $this->changed = true;

        return $this;
    }

    /**
     * Подсчет количества в корзине
     *
     * @return int|float
     */
    public function count()
    {
        return $this->getItems()->sum(function ($item) {
            return $item->getCount();
        });
    }

    /**
     * Очистка корзины
     */
    public function clear()
    {
        $this->cartItems = null;
        Session::remove(config('cart.instanceSessionName'));
        $this->changed = true;
    }

    /**
     * Сумма всего в корзине, форматированный вывод
     *
     * @param null $decimals     количество десятичных знаков
     * @param null $decimalSep   разделитель десятичных знаков
     * @param null $thousandsSep разделитель тысяч
     *
     * @return string
     */
    public function formatTotalPrice(
        $decimals = null,
        $decimalSep = null,
        $thousandsSep = null
    ) {
        return CartHelper::number_format($this->totalPrice(), $decimals,
            $decimalSep, $thousandsSep);
    }

    /**
     * Сумма всего в корзине
     *
     * @return float
     */
    public function totalPrice()
    {
        $total = $this->getItems()->sum(function ($item) {
            /** @var \Mavsan\LaCart\Models\CartItem $item */
            return $item->getTotalPrice();
        });

        return $this->isRound() ? CartHelper::round($total) : $total;
    }

    /**
     * @return bool
     */
    public function isRound()
    {
        return $this->round;
    }

    /**
     * @param bool $round
     *
     * @return $this
     */
    public function setRound($round)
    {
        $round = is_bool($round)
            ? $round : ($round === 1 || $round == '1' ? true : false);

        $this->round = $round;

        return $this;
    }

    /**
     * Форматирование вывода всего со скидкой
     *
     * @param null $decimals     количество символов после запятой
     * @param null $decimalSep   разделитель дробной части
     * @param null $thousandsSep разделитель тысяч
     *
     * @return string
     */
    public function formatTotalDiscount(
        $decimals = null,
        $decimalSep = null,
        $thousandsSep = null
    ) {
        return CartHelper::number_format($this->totalDiscount(), $decimals,
            $decimalSep, $thousandsSep);
    }

    /**
     * Всего со скидкой
     *
     * @return float
     */
    public function totalDiscount()
    {
        $rules = explode(',', config('cart.cartDiscount'));
        $this->initializeCartDiscountRules();

        $total = array_key_exists('productsTotal', $rules)
            ? $this->totalPrice()
            : $this->getItems()->sum(function ($item) {
                /** @var \Mavsan\LaCart\Models\CartItem $item */
                return $item->getTotalDiscount();
            });

        unset($rules['productsTotal']);

        /** @var \Mavsan\LaCart\Models\CartDiscount $discountRules */
        $discountRules = \App::make('cartDiscount');
        $discountRule = $discountRules->getDiscountRule($total);

        foreach ($rules as $rule) {
            if ($rule === 'percent') {
                $total = $total - $total * $discountRule->getPercent() / 100;
            } else if ($rule === 'discount') {
                $total -= $discountRule->getDiscount();
            }
        }

        return $this->isRound() ? CartHelper::round($total) : $total;
    }

    /**
     * Инициализация правил корзины
     */
    protected function initializeCartDiscountRules()
    {
        $callback = config('cart.cartDiscountRules');

        if (! empty($callback)) {
            if ($callback instanceof Closure) {
                call_user_func($callback);
            } elseif (Str::contains($callback, '::')) {
                list($class, $method) = explode('::', $callback, 2);
                call_user_func([\App::make($class), $method]);
            } elseif (Str::contains($callback, '@')) {
                list($class, $method) = Str::parseCallback($callback);
                call_user_func([\App::make($class), $method]);
            } elseif (is_callable($callback)) {
                call_user_func($callback);
            }
        }
    }

    /**
     * Поиск по добавленным
     *
     * @param \Closure $function
     *
     * @return Collection
     */
    public function search(Closure $function)
    {
        return $this->getItems()->filter($function);
    }

    /**
     * Установка модели для CartItem
     *
     * @param string        $rowID ИД нужной записи
     * @param string|Object $model
     *
     * @return \Mavsan\LaCart\Models\CartItem
     */
    public function setModel($rowID, $model)
    {
        $cartItem = $this->getItem($rowID)->setModel($model);

        $this->setItems();

        return $cartItem;
    }

    /**
     * Получение модели элемента корзины
     *
     * @param string $rowID    ИД элемента корзины
     * @param bool   $asObject получить как объект или как строку
     *
     * @return \Illuminate\Database\Eloquent\Model|\Mavsan\LaCart\Interfaces\Buyable|string
     */
    public function getModel($rowID, $asObject = false)
    {
        return $this->getItem($rowID)->getModel($asObject);
    }

    /**
     * Сохранение корзины в базе данных
     *
     * @param string $identifier идентификатор корзины
     * @param bool $updateIfExists обновить, если запись с таким идентификатором уже есть в базе
     */
    public function store($identifier, $updateIfExists = false)
    {
        $db = $this->getDBConnection();

        $isStored = $this->isCartStoredInDB($identifier);

        if ($updateIfExists === false && $isStored) {
            throw new CartAlreadyStoredException(__('cart::cart.cart.storeError'));
        }

        $data = [
            'identifier' => $identifier,
            'type'       => null,
            'order_data' => serialize($this->getItems()),
        ];

        if ($updateIfExists === false || ! $isStored) {
            $db->table('cart')->insert($data);
        } else {
            unset($data['identifier']);
            $db->table('cart')->where('identifier', $identifier)->update($data);
        }

        event('cart.stored');
    }

    /**
     * @return \Illuminate\Database\Connection
     */
    protected function getDBConnection()
    {
        $name = config('cart.dbConnectionName');
        $name = empty($name) ? config('database.default') : $name;

        return \DB::connection($name);
    }

    /**
     * Проверка сохранена ли уже корзина
     *
     * @return bool
     */
    public function isCartStoredInDB($identifier)
    {
        return $this->getDBConnection()->table('cart')
                    ->where('identifier', $identifier)->exists();
    }

    /**
     * Восстановление корзины из базы данных
     *
     * @param $identifier
     */
    public function restore($identifier)
    {
        if (! $this->isCartStoredInDB($identifier)) {
            return;
        }

        $data = $this->getDBConnection()->table('cart')
                     ->where('identifier', $identifier)->first();

        $items = unserialize($data->order_data);

        foreach ($items as $cartItem) {
            /** @var \Mavsan\LaCart\Models\CartItem $cartItem */

            if ($this->getItems()->has($cartItem->getRowID())) {
                $this->updateItem($cartItem->getRowID(), $cartItem);
            } else {
                $this->getItems()->put($cartItem->getRowID(), $cartItem);
            }
        }

        $this->setItems();

        event('cart.restored');
    }

    /**
     * Удаление информации о корзине из базы данных
     *
     * @param $identifier
     */
    public function deleteCartFromDB($identifier)
    {
        $this->getDBConnection()->table('cart')->where('identifier', $identifier)->delete();

        event('cart.deletedFromDB');
    }

    /**
     * Были ли изменения в корзине
     *
     * @return bool
     */
    public function isChanged()
    {
        return $this->changed;
    }
}