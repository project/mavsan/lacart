<?php
/**
 * Item.php
 * Date: 01.08.2017
 * Time: 9:50
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Models;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;
use InvalidArgumentException;
use Mavsan\LaCart\Exceptions\CartItemModelNotFoundException;
use Mavsan\LaCart\Helper\CartHelper;
use Mavsan\LaCart\Interfaces\Buyable;

class CartItem implements Arrayable, Jsonable
{
    /** @var  string внутренний идентификатор */
    protected $rowID;

    /** @var string|int ид товара */
    protected $id;

    /** @var string название товара */
    protected $title = '';

    /** @var float стоимость товара */
    protected $price = 0;

    /** @var float скидка для товара */
    protected $discount = 0;

    /** @var float скида для товара в % */
    protected $discountPercent = 0;

    /** @var array опции товара - цвет, размер и т.д. */
    protected $options = [];

    /** @var int кол-во */
    protected $count = 0;

    /** @var  string класс модели товара (не экземпляр!) */
    protected $model;

    /** @var bool округлять или не округлять результат */
    protected $round = false;

    /** @var bool генерировать, или нет событие о том, что элемент корзины был обновлен */
    protected $genEventUpdated = true;

    /**
     * Item constructor.
     *
     * @param string $id
     * @param string $title
     * @param int    $price
     * @param int    $count
     * @param array  $options
     */
    public function __construct($id, $title, $price, $count, $options)
    {
        $this->genEventUpdated = false;

        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->setCount($count);
        $this->options = $options;

        $this->generateRowID();

        $this->genEventUpdated = true;
    }

    /**
     * Генерирование ИД строки
     */
    protected function generateRowID()
    {
        ksort($this->options);

        $this->rowID = md5($this->id.serialize($this->options));
    }

    /**
     * Создание экземпляра из модели товара, реализующего интерфейс Buyable
     *
     * @param \Mavsan\LaCart\Interfaces\Buyable $model
     * @param int                               $count
     * @param array                             $options
     *
     * @return \Mavsan\LaCart\Models\CartItem
     */
    public static function createFromBuyable(
        Buyable $model,
        $count,
        array $options = []
    )
    {
        return new self(
            $model->cartGetID(),
            $model->cartGetTitle(),
            $model->cartGetPrice(),
            $count,
            $options
        );
    }

    /**
     * Создание экземпляра из массива
     *
     * @param array $data
     *
     * @return \Mavsan\LaCart\Models\CartItem
     */
    public static function createFromArray(array $data)
    {
        return new self(
            $data['id'],
            $data['title'],
            $data['price'],
            $data['count'],
            Arr::get($data, 'options', [])
        );
    }

    /**
     * Обновление данных из модели
     *
     * @param \Mavsan\LaCart\Interfaces\Buyable $model
     *
     * @return $this
     */
    public function updateFromBuyable(Buyable $model)
    {
        $this->genEventUpdated = false;

        $this->setId($model->cartGetID());
        $this->setTitle($model->cartGetTitle());
        $this->setPrice($model->cartGetPrice());
        $this->setModel($model);

        $this->genEventUpdated = true;

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * Обновление данных из массива
     *
     * @param $data
     *
     * @return $this
     */
    public function updateFromArray($data)
    {
        $this->genEventUpdated = false;

        $this->setId(Arr::get($data, 'id', $this->id));
        $this->setTitle(Arr::get($data, 'title', $this->title));
        $this->setPrice(Arr::get($data, 'price', $this->price));
        $this->setCount(Arr::get($data, 'count', $this->count));
        $this->setOptions(Arr::get($data, 'options', $this->options));

        $this->genEventUpdated = true;

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * Получение отформатированной стоимости (number_format), если какой-либо
     * из аргументов не передан - значение будет взято из файла конфигурации
     *
     * @param null $decimals     кол-во десятичных знаков
     * @param null $decPoint     разделитель десятичных знаков
     * @param null $thousandsSep разделитель тысяч
     *
     * @return string
     */
    public function formatPrice(
        $decimals = null,
        $decPoint = null,
        $thousandsSep = null
    ) {
        return CartHelper::number_format(
            $this->getPrice(),
            $decimals,
            $decPoint,
            $thousandsSep
        );
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     *
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'rowID'           => $this->rowID,
            'id'              => $this->id,
            'title'           => $this->title,
            'price'           => $this->price,
            'discount'        => $this->discount,
            'discountPercent' => $this->discountPercent,
            'options'         => $this->options,
            'count'           => $this->count,
            'model'           => $this->model,
            'round'           => $this->round,
        ];
    }

    /**
     * @return string
     */
    public function getRowID()
    {
        return $this->rowID;
    }

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Обновлять нельзя, от него зависит rowID
     *
     * @param int|string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        $this->generateRowID();

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     *
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * @return float
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * @param float $discountPercent
     *
     * @return $this
     */
    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Обновлять нельзя, от него зависит rowID
     *
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        $this->generateRowID();

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setCount($count)
    {
        if (! is_numeric($count) || $count < 0) {
            throw new InvalidArgumentException(__('cart::cart.cartItem.invalidCountValue'));
        }

        $this->count = $count;

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|Buyable|string
     * @throws CartItemModelNotFoundException
     */
    public function getModel($asObject = false)
    {
        if ($asObject) {
            if (empty($this->model)) {
                throw new CartItemModelNotFoundException(__('cart::cart.cartItem.modelNotSet'));
            }

            return (new $this->model)->find($this->id);
        }

        return $this->model;
    }

    /**
     * @param string $model
     *
     * @return $this
     */
    public function setModel($model)
    {
        if (is_string($model) && class_exists($model)) {
            $this->model = $model;
        } elseif (is_object($model)) {
            $this->model = get_class($model);
        } else {
            throw new CartItemModelNotFoundException();
        }

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * Получение отформатированной суммы всего
     *
     * @param null $decimals
     * @param null $decimalSep
     * @param null $thousandsSep
     *
     * @return string
     */
    public function formatTotalPrice(
        $decimals = null,
        $decimalSep = null,
        $thousandsSep = null
    ) {
        return CartHelper::number_format(
            $this->getTotalPrice(),
            $decimals,
            $decimalSep,
            $thousandsSep
        );
    }

    /**
     * Получение суммы - всего
     *
     * @return float
     */
    public function getTotalPrice()
    {
        $total = $this->count * $this->price;

        return $this->isRound() ? CartHelper::round($total) : $total;
    }

    /**
     * @return bool
     */
    public function isRound()
    {
        return $this->round;
    }

    /**
     * @param bool $round
     *
     * @return $this
     */
    public function setRound($round)
    {
        $round = is_bool($round)
            ? $round : ($round === 1 || $round == '1' ? true : false);

        $this->round = $round;

        $this->generateItemUpdatedEvent();

        return $this;
    }

    /**
     * Форматирование стоимости (без учета количества) с учетом скидки
     *
     * @param null $decimals     кол-во цифр после запятой
     * @param null $decimalSep   разделитель дробной части
     * @param null $thousandsSep разделитель тысяч
     *
     * @return string
     */
    public function formatPriceDiscount(
        $decimals = null,
        $decimalSep = null,
        $thousandsSep = null
    ) {
        return CartHelper::number_format(
            $this->getPriceDiscount(),
            $decimals,
            $decimalSep,
            $thousandsSep
        );
    }

    /**
     * Получение стоимости (без учета количества) с учетом скидки
     * @return float
     */
    public function getPriceDiscount()
    {
        $rules = explode(',', config('cart.itemDiscount'));

        $rules = count($rules) <= 2 ? $rules : array_slice($rules, 0, 2);

        $price = $this->price;

        foreach ($rules as $rule) {
            if ($rule === 'percent') {
                $price = $this->calcDiscountPercent($price);
            } elseif ($rule === 'discount') {
                $price = $this->calcDiscount($price);
            }
        }

        return $this->isRound() ? CartHelper::round($price) : $price;
    }

    /**
     * Подсчет скидки в %
     *
     * @param float $price
     *
     * @return float
     */
    protected function calcDiscountPercent($price)
    {
        $discount = $price - $price * $this->discountPercent / 100;

        return $this->isRound() ? CartHelper::round($discount) : $discount;
    }

    /**
     * Подсчет стоимости с фиксированной скидкой
     *
     * @param float $price
     *
     * @return float
     */
    protected function calcDiscount($price)
    {
        $discount = $price - $this->discount;

        return $this->isRound() ? CartHelper::round($discount) : $discount;
    }

    public function formatTotalDiscount(
        $decimals = null,
        $decimalSep = null,
        $thousandsSep = null
    ) {
        return CartHelper::number_format(
            $this->getTotalDiscount(),
            $decimals,
            $decimalSep,
            $thousandsSep
        );
    }

    /**
     * Получение полной стоимости с учетом количества и скидки
     *
     * @return float
     */
    public function getTotalDiscount()
    {
        $total = $this->getPriceDiscount() * $this->count;

        return $this->isRound() ? CartHelper::round($total) : $total;
    }

    protected function generateItemUpdatedEvent()
    {
        if ($this->genEventUpdated) {
            event('cart.itemUpdated', $this);
        }
    }
}
