<?php
/**
 * CartRepository.php
 * Date: 02.08.2017
 * Time: 11:37
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Helper;

class CartHelper
{
    /**
     * Форматирование циферок (number_format)
     *
     * @param number $number       сумма, требующая форматирования
     * @param null   $decimals     кол-во знаков после запятой
     * @param null   $decPoint     разделитель копеек
     * @param null   $thousandsSep разделитель тысяч
     *
     * @return string
     */
    public static function number_format(
        $number,
        $decimals = null,
        $decPoint = null,
        $thousandsSep = null
    ) {
        $decimals = $decimals ?: config('cart.format.decimals');
        $decPoint = $decPoint ?: config('cart.format.dec_point');
        $thousandsSep = $thousandsSep ?: config('cart.format.thousands_sep');

        return number_format($number, $decimals, $decPoint, $thousandsSep);
    }

    /**
     * Округление
     *
     * @param number $number
     *
     * @return float
     */
    public static function round($number)
    {
        return round(
            $number,
            config('cart.priceRound.precision', 0),
            config('cart.priceRound.mode', PHP_ROUND_HALF_UP)
        );
    }
}
