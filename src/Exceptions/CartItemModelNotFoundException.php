<?php
/**
 * CartItemModelNotFoundException.php
 * Date: 01.08.2017
 * Time: 15:46
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Exceptions;

use RuntimeException;

class CartItemModelNotFoundException extends RuntimeException
{
}
