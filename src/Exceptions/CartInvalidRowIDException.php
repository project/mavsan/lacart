<?php
/**
 * CartInvalidRowIDException.php
 * Date: 01.08.2017
 * Time: 13:48
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Exceptions;

use RuntimeException;

class CartInvalidRowIDException extends RuntimeException
{
}
