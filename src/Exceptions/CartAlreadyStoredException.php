<?php
/**
 * CartAlreadyStoredException.php
 * Date: 04.08.2017
 * Time: 15:51
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Exceptions;

use RuntimeException;

class CartAlreadyStoredException extends RuntimeException
{
}
