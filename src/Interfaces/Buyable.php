<?php
/**
 * Buyable.php
 * Date: 01.08.2017
 * Time: 10:35
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaCart\Interfaces;

interface Buyable
{
    /**
     * Получение идентификатора товара
     * @return int|string
     */
    public function cartGetID();

    /**
     * Получение названия товара
     * @return string
     */
    public function cartGetTitle();

    /**
     * Получение стоимости товара
     * @return double
     */
    public function cartGetPrice();
}
