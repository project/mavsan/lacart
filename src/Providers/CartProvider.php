<?php

namespace Mavsan\LaCart\Providers;

use App;
use Illuminate\Support\ServiceProvider;

class CartProvider extends ServiceProvider
{
    protected $root = '';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //        if (\App::runningInConsole() ||  $this->app->environment('testing')) {
        $this->loadMigrationsFrom($this->getPackageRoot('database/migrations'));
        //        }

        $this->publishes([
            $this->getPackageRoot('config/cart.php') => config_path('cart.php'),
            $this->getPackageRoot('i18n')            => resource_path('lang/vendor/cart'),
        ]);

        $this->loadTranslationsFrom($this->getPackageRoot('i18n'), 'cart');
    }

    /**
     * Путь к корню пакета
     *
     * @param string $path
     *
     * @return string
     */
    protected function getPackageRoot($path = '')
    {
        if ($this->root === '') {
            $this->root =
                realpath(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR
                         .'..');
        }

        return empty($path) ? $this->root
            : $this->root.DIRECTORY_SEPARATOR.$path;
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('cart', 'Mavsan\LaCart\Models\Cart');
        $this->app->singleton(
            'cartDiscount',
            'Mavsan\LaCart\Models\CartDiscount'
        );

        $this->mergeConfigFrom(
            $this->getPackageRoot('config/cart.php'),
            'cart'
        );

        $this->app['events']->listen(
            'Illuminate\Auth\Events\Logout',
            function () {
                if (config('cart.clearCartOnLogout', false)) {
                    App::make('cart')->clear();
                }
            }
        );
    }
}
